class User < ActiveRecord::Base
    has_many :statuses
    has_many :user_friendships
    has_many :friends, through: :user_friendships
   
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
   attr_accessible :email, :password, :password_confirmation, :remember_me, :first_name, :last_name, :profile_name
   validates :first_name, presence: true,
                           length: { in: 3..20 }
   
   validates :last_name, presence: true
   validates :profile_name, presence: true,
                            length: { in: 4..20 },
                            uniqueness: true,
                            format:  {with: /^[a-zA-Z0-9_-]+$/,
                                      message: 'format input correctly.'
                                     }

    
    def full_name
  	 first_name + " " + last_name
    end
    
    def gravatar_url
      stripped_email = email.strip
      downcased_email = stripped_email.downcase
      hash = Digest::MD5.hexdigest(downcased_email)
      "http://gravatar.com/avatar/#{hash}"
    end
end
 