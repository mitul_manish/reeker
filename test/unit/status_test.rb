require 'test_helper'

class StatusTest < ActiveSupport::TestCase
  test "a status must have a user id attached to it" do
    status = Status.new
    status.content = "This status does not contain a user id"
    assert !status.save
    assert !status.errors[:user_id].empty?
   end
   
   test "the content of the status must not be empty" do
     status = Status.new
     status.content = ""
     assert !status.save
     assert !status.errors[:content].empty?
   end
   
   test "the content of the status must not be smaller than 4 characters" do
     status = Status.new
     status.content = "hie"
     assert !status.save
     assert !status.errors[:content].empty?
   end
   
   test "the content of the status must not be larger than 500 characters" do
     status = Status.new
     status.content = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut molestie ut turpis non vestibulum. Maecenas mollis ipsum convallis, gravida lorem eu, ornare erat. Interdum et malesuada fames ac ante ipsum primis in faucibus. Suspendisse vehicula libero eget ipsum vehicula euismod. Nulla finibus pharetra lacus eu scelerisque. Nulla facilisi. Integer nisl neque, feugiat vitae ante vitae, posuere consequat risus. Suspendisse quis fermentum turpis, id auctor nibh. Suspendisse potenti.

Maecenas finibus quis turpis quis sodales. Phasellus et dui dolor. Pellentesque faucibus ligula augue, quis finibus mi commodo a. Suspendisse gravida mauris ut lacus blandit, fermentum rhoncus dui malesuada. Ut erat est, tincidunt et tempor id, faucibus sit amet dui. Donec consequat ante a sapien laoreet elementum. Vivamus mattis pellentesque augue, ac ullamcorper lectus suscipit id. Quisque nec velit vel ante gravida mattis. Vestibulum ultrices odio non nisi pellentesque vestibulum. Curabitur dapibus bibendum orci eget porta. Maecenas facilisis lacinia odio, at maximus orci consequat vulputate. Donec quis nulla leo. Mauris vel metus eros. Nulla ac semper lectus, ac hendrerit orci. In hac habitasse platea dictumst.

Quisque vehicula molestie dolor ac ornare. Vestibulum rhoncus nulla eget semper condimentum. Quisque sit amet justo eu augue sodales consectetur. In ut tempus eros, in eleifend sapien. In hendrerit sapien purus, ut hendrerit mi consequat eget. Curabitur in porta justo. Mauris risus tellus, congue in tincidunt vitae, luctus quis justo. Nunc viverra gravida sem ut dignissim. Mauris ut porttitor ipsum. Nulla blandit pretium faucibus. Phasellus molestie, nulla in lacinia tempus, elit velit mattis nunc, nec volutpat est metus id libero.

In ut nisl bibendum magna fringilla dapibus eu rutrum enim. Phasellus tellus dolor, gravida at volutpat sed, egestas at turpis. Sed lacinia viverra elit eget viverra. Aenean a auctor tortor. Vestibulum condimentum orci felis, ac luctus justo dignissim eu. Suspendisse vitae rhoncus quam. Maecenas ligula lectus, pretium sollicitudin nisl eget, elementum vehicula nibh. Ut tincidunt purus et quam consequat, ut faucibus lorem auctor. Nam elit dolor, scelerisque nec sem tincidunt, faucibus bibendum elit. Phasellus congue lacus felis, a semper augue fermentum ac. Sed mattis efficitur turpis, at auctor dui luctus vitae. Donec nec nibh urna. Sed hendrerit enim ipsum, sit amet auctor erat vulputate et. Fusce fermentum risus eget cursus posuere. Donec at lacus hendrerit, egestas sem fermentum, aliquam dui.

Nulla egestas tempor varius. Quisque diam turpis, efficitur a dignissim ac, sodales eu nisi. Maecenas condimentum eros ut sollicitudin lobortis. In elit elit, consectetur eget augue quis, sollicitudin luctus tortor. Curabitur placerat purus nisi, mollis pellentesque quam dictum eget. Quisque ut mattis odio. Aenean non tortor est. Cras odio leo, dapibus egestas venenatis mattis, feugiat eget nulla. Suspendisse potenti. Sed sagittis elit massa, vitae finibus purus convallis ac.

Nullam volutpat leo ut congue feugiat. Maecenas sit amet bibendum nulla. Cras eu purus felis. Proin euismod consequat nulla sit amet suscipit. Proin nec sodales lectus, ac sagittis dolor. Donec aliquet pulvinar odio, eget aliquet quam venenatis ut. Morbi tempus purus id nibh molestie euismod.

Nulla nibh velit, sagittis in quam a, eleifend viverra libero. Proin mauris neque, venenatis dictum eros et, sollicitudin elementum nulla. Mauris rutrum posuere diam, non hendrerit ligula consequat nec. Phasellus ut ante sed dolor elementum ornare. Maecenas egestas leo condimentum, suscipit nisl ut, aliquet risus. Pellentesque id congue leo. Sed consectetur massa at ante ultrices consequat egestas nec lorem. Duis congue leo sit amet nulla egestas malesuada. Nunc faucibus velit id gravida aliquam. In gravida posuere nunc eget posuere. Phasellus sed velit sed velit pulvinar feugiat. Pellentesque libero ante, fermentum eu eleifend eu, pretium sit amet turpis. Morbi lacus enim, malesuada a nisl at, laoreet mollis urna. Nam ultrices, enim a bibendum eleifend, ex purus tincidunt leo, nec rhoncus leo velit et purus.

"
     assert !status.save
     assert !status.errors[:content].empty?
   end
end
