require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "a user should enter a first name" do
    user = User.new
    user.first_name = ""
    assert !user.save
    assert !user.errors[:first_name].empty?
   end
   
  
     should have_many(:user_friendships)
     should have_many(:friends)
 
   test "a user should enter a last name" do
     u1 = User.new
     u1.last_name = ""
     assert !u1.save
     assert !u1.errors[:last_name].empty?
   end
   
   test "a user should have  a profile name" do 
     u2 = User.new
     u2.profile_name = ""
     assert !u2.save
     assert !u2.errors[:profile_name].empty?   
   end
   test "a user should have a unique profile name" do
      user = User.new
      user.profile_name = users(:jason).profile_name
    
      assert !user.save
      assert !user.errors[:profile_name].empty?
    end
 
    
    test "a profile name should not be too large" do
        user = User.new
        user.profile_name = "AnnaHathawaythemillers"
        assert !user.save
        assert !user.errors[:profile_name].empty?
    end
    
    test "a first name should not be be smaller than 3 characters" do 
      user = User.new
      user.first_name = "Ki"
      assert !user.save
      assert !user.errors[:first_name].empty?
    end
    
    test "that no error is raised when trying to access a friend list " do
      assert_nothing_raised do
        users(:jason).friends
      end
    end
    
    test "creating a friendship on a user works" do
      users(:jason).friends << users(:mike)
      users(:jason).friends.reload
      assert users(:jason).friends.include?(users(:mike))
    end
    
    test "that jim is a friend of jason's" do
      assert users(:jason).friends.include?(users(:jim))
    end
    
   
end
