require 'test_helper'

class CustomRoutesTest < ActionDispatch::IntegrationTest
   test "the /login route takes page to the login page" do
   get '/login'   
   assert_response :success
   end
   
   test "the /logout routes takes page to the logout" do
   get '/logout'
   assert_response :redirect 
   assert_redirected_to '/'
   end
   
   test "the /register route takes page to the registration page" do
     get '/register'
     assert_response :success
   end
   
   test "that the name routes work properly" do
     get "/jasonseifer"
     assert_response :success
   end
end
